﻿using Newtonsoft.Json;
using System;
using System.Reflection;
using System.Text;
using TaskManager.Common.Enums;
using TaskManager.Data.DataAccess;

namespace TaskManager.Data.Entities
{
    public class Task: IEntity
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }
        [JsonProperty("finishedAt")]
        public DateTime FinishedAt { get; set; }
        [JsonProperty("state")]
        public TaskState State { get; set; }

        [JsonProperty("projectId")]
        public int ProjectId { get; set; }
        public Project Project { get; set; }

        [JsonProperty("performerId")]
        public int PerformerId { get; set; }
        public User Performer { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            var properties = typeof(Task).GetProperties();
            foreach (PropertyInfo property in properties)
            {
                sb.AppendLine(string.Format("{0}: {1}", property.Name, property.GetValue(this)));
            }

            sb.AppendLine();
            return sb.ToString();
        }
    }
}

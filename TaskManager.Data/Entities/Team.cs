﻿using Newtonsoft.Json;
using System;
using TaskManager.Data.DataAccess;

namespace TaskManager.Data.Entities
{
    public class Team: IEntity
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using TaskManager.Data.DataAccess;

namespace TaskManager.Data.Entities
{
    public class Project: IEntity
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }
        [JsonProperty("deadline")]
        public DateTime Deadline { get; set; }

        [JsonProperty("authorId")]
        public int AuthorId { get; set; }
        public User Author { get; set; }

        [JsonProperty("teamId")]
        public int TeamId { get; set; }
        public Team Team { get; set; }

        public List<Task> Tasks { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            var properties = typeof(Project).GetProperties();
            foreach (PropertyInfo property in properties)
            {
                sb.AppendLine(string.Format("{0}: {1}", property.Name, property.GetValue(this).ToString()));
            }

            sb.AppendLine();
            return sb.ToString();
        }
    }
}
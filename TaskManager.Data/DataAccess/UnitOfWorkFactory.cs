﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using TaskManager.Data.DbContexts;

namespace TaskManager.Data.DataAccess
{
    public class UnitOfWorkFactory: IUnitOfWorkFactory
    {
        public IUnitOfWork Create(TaskManagerContext dataContext)
        {
            return new UnitOfWork(dataContext);
        }
    }
}

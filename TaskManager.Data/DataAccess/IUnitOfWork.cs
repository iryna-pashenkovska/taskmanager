﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace TaskManager.Data.DataAccess
{
    public interface IUnitOfWork: IDisposable
    { 
        IRepository<T> Repository<T>() where T : class, IEntity;
        void SaveChanges();
    }
}

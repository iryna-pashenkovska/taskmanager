﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskManager.Data.DataAccess
{
    public interface IRepository<T> where T: class, IEntity
    {
        IQueryable<T> Query();
        Task<IEnumerable<T>> All();
        T Get(int id);
        T Get(Func<T, bool> predicate);
        void Add(T entity);
        void Delete(T entity);
    }
}

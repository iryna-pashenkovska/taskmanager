﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskManager.Data.DataAccess
{
    public class Repository<T>: IRepository<T> where T: class, IEntity
    {
        protected readonly DbContext DataContext;
        protected readonly DbSet<T> DataSet;

        public Repository(DbContext dataContext)
        {
            DataContext = dataContext;
            DataSet = dataContext.Set<T>();
        }

        public IQueryable<T> Query()
        {
            return DataSet;
        }

        public async Task<IEnumerable<T>> All()
        {
            return await DataSet.AsNoTracking().ToListAsync();
        }

        public T Get(int id)
        {
            return DataSet.FirstOrDefault(x => x.Id == id);
        }

        public T Get(Func<T,bool> predicate)
        {
            return DataSet.FirstOrDefault(predicate);
        }

        public void Add(T entity)
        {
            DataSet.Add(entity);
        }

        public void Delete(T entity)
        {
            DataSet.Remove(entity);
        }

        public void SaveChanges()
        {
            DataContext.SaveChangesAsync();
        }
    }
}

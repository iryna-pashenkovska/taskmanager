﻿using System;
using System.Collections.Generic;
using System.Text;
using TaskManager.Data.DbContexts;

namespace TaskManager.Data.DataAccess
{
    public interface IUnitOfWorkFactory
    {
        IUnitOfWork Create(TaskManagerContext dataContext);
    }
}

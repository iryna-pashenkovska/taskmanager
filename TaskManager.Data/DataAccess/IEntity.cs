﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskManager.Data.DataAccess
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}

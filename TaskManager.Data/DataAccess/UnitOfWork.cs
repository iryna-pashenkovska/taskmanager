﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using TaskManager.Data.DbContexts;

namespace TaskManager.Data.DataAccess
{
    public class UnitOfWork: IUnitOfWork
    {
        private readonly TaskManagerContext _dataContext;

        public UnitOfWork(TaskManagerContext dataContext)
        {
            _dataContext = dataContext;
        }

        public IRepository<T> Repository<T>() where T : class, IEntity
        {
            return new Repository<T>(_dataContext);
        }

        public void SaveChanges()
        {
            _dataContext.SaveChangesAsync();
        }

        public void Dispose()
        {
            _dataContext.Dispose();
        }
    }
}

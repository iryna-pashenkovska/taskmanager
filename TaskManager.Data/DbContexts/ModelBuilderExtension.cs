﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using TaskManager.Data.Entities;
using Bogus;
using TaskManager.Common.Enums;

namespace TaskManager.Data.DbContexts
{
    public static class ModelBuilderExtension
    {
        private const int TEAM_ENTITY_COUNT = 10;
        private const int USER_ENTITY_COUNT = 50;
        private const int PROJECT_ENTITY_COUNT = 20;
        private const int TASK_ENTITY_COUNT = 200;
        private const int MINIMUM_USER_AGE = 20;
        private const int MAXIMUM_USER_AGE = 60;
        private const int N_YEARS_AGO = 2;

        private static DateTime minimumBirthdayDate = DateTime.Now.AddYears(-MINIMUM_USER_AGE);
        private static DateTime maximumBirthdayDate = DateTime.Now.AddYears(-MAXIMUM_USER_AGE);

        public static void Configure(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Project>()
                .HasMany<Task>(p => p.Tasks)
                .WithOne(t => t.Project)
                .OnDelete(DeleteBehavior.Restrict);
        }

        public static void Seed(this ModelBuilder modelBuilder)
        {
            var teams = GenerateRandomTeams();
            var users = GenerateRandomUsers();
            var projects = GenerateRandomProjects();
            var tasks = GenerateRandomTasks();

            modelBuilder.Entity<Team>().HasData(teams);
            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<Task>().HasData(tasks);
        }

        public static ICollection<Team> GenerateRandomTeams()
        {
            int teamId = 1;

            var teamFake = new Faker<Team>()
                .RuleFor(t => t.Id, f => teamId++)
                .RuleFor(t => t.Name, f => f.Lorem.Word())
                .RuleFor(t => t.CreatedAt, f => f.Date.Past(N_YEARS_AGO, minimumBirthdayDate));

            return teamFake.Generate(TEAM_ENTITY_COUNT);
        }

        public static ICollection<User> GenerateRandomUsers()
        {
            int userId = 1;

            var userFake = new Faker<User>()
                .RuleFor(u => u.Id, f => userId++)
                .RuleFor(u => u.FirstName, f => f.Name.FirstName())
                .RuleFor(u => u.LastName, f => f.Name.LastName())
                .RuleFor(u => u.Email, (f, u) => f.Internet.Email(u.FirstName, u.LastName))
                .RuleFor(u => u.Birthday, f => f.Date.Between(maximumBirthdayDate, minimumBirthdayDate))
                .RuleFor(u => u.RegisteredAt, f => f.Date.Past(N_YEARS_AGO, minimumBirthdayDate))
                .RuleFor(u => u.TeamId, f => f.Random.Int(1, TEAM_ENTITY_COUNT));

            return userFake.Generate(USER_ENTITY_COUNT);
        }

        public static ICollection<Project> GenerateRandomProjects()
        {
            int projectId = 1;

            var projectFake = new Faker<Project>()
                .RuleFor(p => p.Id, f => projectId++)
                .RuleFor(p => p.Name, f => f.Lorem.Word())
                .RuleFor(p => p.Description, f => f.Lorem.Sentences(f.Random.Int(1, 5)))
                .RuleFor(p => p.CreatedAt, f => f.Date.Past(N_YEARS_AGO, minimumBirthdayDate))
                .RuleFor(p => p.Deadline, (f, p) => f.Date.Between(p.CreatedAt, p.CreatedAt.AddDays(f.Random.Int(10,100))))
                .RuleFor(p => p.AuthorId, f => f.Random.Int(1, USER_ENTITY_COUNT))
                .RuleFor(p => p.TeamId, f => f.Random.Int(1, TEAM_ENTITY_COUNT));

            return projectFake.Generate(PROJECT_ENTITY_COUNT); 
        }

        public static ICollection<Task> GenerateRandomTasks()
        {
            int taskId = 1;

            var taskFake = new Faker<Task>()
                .RuleFor(t => t.Id, f => taskId++)
                .RuleFor(t => t.Name, f => f.Lorem.Word())
                .RuleFor(t => t.Description, f => f.Lorem.Sentences(f.Random.Int(1, 5)))
                .RuleFor(t => t.CreatedAt, f => f.Date.Past(N_YEARS_AGO, minimumBirthdayDate))
                .RuleFor(t => t.FinishedAt, (f, t) => f.Date.Between(t.CreatedAt, t.CreatedAt.AddDays(f.Random.Int(10, 100))))
                .RuleFor(t => t.State, f => f.Random.Enum<TaskState>())
                .RuleFor(p => p.ProjectId, f => f.Random.Int(1, PROJECT_ENTITY_COUNT))
                .RuleFor(p => p.PerformerId, f => f.Random.Int(1, USER_ENTITY_COUNT));

            return taskFake.Generate(TASK_ENTITY_COUNT);
        }
    }
}

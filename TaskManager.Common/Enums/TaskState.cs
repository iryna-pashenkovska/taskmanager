﻿namespace TaskManager.Common.Enums
{
    public enum TaskState
    {
        Created = 0,
        Started,
        Finished,
        Canceled
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskManager.Common.QueueServiceBuilder
{
    public class ConnectionFactory: RabbitMQ.Client.ConnectionFactory
    {
        public ConnectionFactory(Uri uri)
        {
            Uri = uri;
            RequestedConnectionTimeout = 30000;
            NetworkRecoveryInterval = TimeSpan.FromSeconds(30);
            AutomaticRecoveryEnabled = true;
            TopologyRecoveryEnabled = true;
            RequestedHeartbeat = 60;
        }
    }
}

﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace TaskManager.Common.QueueServiceBuilder.Models
{
    public class MessageScopeSettings
    {
        public string ExchangeName { get; set; }
        public string QueueName { get; set; }
        public string RoutingKey { get; set; }
        public string ExchangeType { get; set; }
        public IModel Channel { get; set; }
    }
}

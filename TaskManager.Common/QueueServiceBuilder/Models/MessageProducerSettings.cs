﻿using System;
using System.Collections.Generic;
using System.Text;
using RabbitMQ.Client;

namespace TaskManager.Common.QueueServiceBuilder.Models
{
    public class MessageProducerSettings
    {
        public PublicationAddress PublicationAddress { get; set; }
        public IModel Channel { get; set; }
    }
}

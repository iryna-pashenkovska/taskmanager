﻿using System;
using System.Collections.Generic;
using System.Text;
using RabbitMQ.Client;

namespace TaskManager.Common.QueueServiceBuilder.Models
{
    public class MessageConsumerSettings
    {
        public bool SequentialFetch { get; set; } = true;
        public bool AutoAcknowledge { get; set; } = false;
        public IModel Channel { get; set; }
        public string QueueName { get; set; }
    }
}

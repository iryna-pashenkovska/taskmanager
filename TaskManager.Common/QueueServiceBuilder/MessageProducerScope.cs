﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;
using TaskManager.Common.QueueServiceBuilder.Interfaces;
using TaskManager.Common.QueueServiceBuilder.Models;

namespace TaskManager.Common.QueueServiceBuilder
{
    public class MessageProducerScope : IMessageProducerScope
    {
        private readonly MessageScopeSettings _messageScopeSettings;
        private readonly Lazy<IMessageQueueBuilder> _messageQueueLazy;
        private readonly Lazy<IMessageProducer> _messageProducerLazy;

        private readonly IConnectionFactory _connectionFactory;

        public MessageProducerScope(IConnectionFactory connectionFactory, MessageScopeSettings messageScopeSettings)
        {
            _connectionFactory = connectionFactory;
            _messageScopeSettings = messageScopeSettings;

            _messageQueueLazy = new Lazy<IMessageQueueBuilder>(CreateMessageQueue);
            _messageProducerLazy = new Lazy<IMessageProducer>(CreateMessageProducer);
        }

        public IMessageProducer MessageProducer => _messageProducerLazy.Value;

        public IMessageQueueBuilder MessageQueueBuilder => _messageQueueLazy.Value;

        private IMessageProducer CreateMessageProducer()
        {
            return new MessageProducer(new MessageProducerSettings
            {
                Channel = MessageQueueBuilder.Channel,
                PublicationAddress = new PublicationAddress(
                    _messageScopeSettings.ExchangeType,
                    _messageScopeSettings.ExchangeName,
                    _messageScopeSettings.RoutingKey)
            });
        }

        private IMessageQueueBuilder CreateMessageQueue()
        {
            return new MessageQueueBuilder(_connectionFactory, _messageScopeSettings);
        }

        public void Dispose()
        {
            MessageQueueBuilder?.Dispose();
        }
    }
}

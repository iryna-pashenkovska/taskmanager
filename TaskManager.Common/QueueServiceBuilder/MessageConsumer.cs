﻿using System;
using System.Collections.Generic;
using System.Text;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using TaskManager.Common.QueueServiceBuilder.Interfaces;
using TaskManager.Common.QueueServiceBuilder.Models;

namespace TaskManager.Common.QueueServiceBuilder
{
    public class MessageConsumer: IMessageConsumer
    {
        private readonly MessageConsumerSettings _messageConsumerSettings;
        private readonly EventingBasicConsumer _consumer;

        public event EventHandler<BasicDeliverEventArgs> Received
        {
            add => _consumer.Received += value;
            remove => _consumer.Received -= value;
        }

        public MessageConsumer(MessageConsumerSettings messageConsumerSettings)
        {
            _messageConsumerSettings = messageConsumerSettings;

            _consumer = new EventingBasicConsumer(_messageConsumerSettings.Channel);
        }

        public void Connect()
        {
            if (_messageConsumerSettings.SequentialFetch)
            {
                _messageConsumerSettings.Channel.BasicQos(0,1,false);
            }

            _messageConsumerSettings.Channel.BasicConsume(_messageConsumerSettings.QueueName,
                _messageConsumerSettings.AutoAcknowledge, _consumer);
        }

        public void SetAcknowledge(ulong deliveryTag, bool processed)
        {
            if (processed)
            {
                _messageConsumerSettings.Channel.BasicAck(deliveryTag, false);
            }
            else
            {
                _messageConsumerSettings.Channel.BasicNack(deliveryTag, false, true);
            }
        }
    }
}

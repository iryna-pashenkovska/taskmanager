﻿using RabbitMQ.Client;

namespace TaskManager.Common.QueueServiceBuilder.Interfaces
{
    public interface IMessageQueueBuilder
    {
        IModel Channel { get; }
        void Dispose();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using TaskManager.Common.QueueServiceBuilder.Models;

namespace TaskManager.Common.QueueServiceBuilder.Interfaces
{
    public interface IMessageConsumerScopeFactory
    {
        IMessageConsumerScope Open(MessageScopeSettings messageScopeSettings);
        IMessageConsumerScope Connect(MessageScopeSettings messageScopeSettings);
    }
}

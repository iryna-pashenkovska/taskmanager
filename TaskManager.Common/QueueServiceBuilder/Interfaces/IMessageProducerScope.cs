﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskManager.Common.QueueServiceBuilder.Interfaces
{
    public interface IMessageProducerScope
    {
        IMessageProducer MessageProducer { get; }
        void Dispose();
    }
}

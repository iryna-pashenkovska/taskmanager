﻿using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;
using TaskManager.Common.QueueServiceBuilder.Models;

namespace TaskManager.Common.QueueServiceBuilder.Interfaces
{
    public interface IMessageConsumer
    {
        void Connect();
        void SetAcknowledge(ulong deliveryTag, bool processed);
        event EventHandler<BasicDeliverEventArgs> Received;
    }
}

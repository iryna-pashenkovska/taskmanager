﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskManager.Common.QueueServiceBuilder.Interfaces
{
    public interface IMessageProducer
    {
        void Send(string message, string type = null);
        void SendType(Type type, string message);
    }
}

﻿using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace TaskManager.Common.QueueServiceBuilder.Interfaces
{
    public interface IMessageConsumerScope
    {
        void Dispose();
        IMessageConsumer MessageConsumer { get; }
    }
}

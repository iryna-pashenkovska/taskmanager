﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;
using TaskManager.Common.QueueServiceBuilder.Interfaces;
using TaskManager.Common.QueueServiceBuilder.Models;

namespace TaskManager.Common.QueueServiceBuilder
{
    public class MessageProducerScopeFactory: IMessageProducerScopeFactory
    {
        private readonly IConnectionFactory _connectionFactory;

        public MessageProducerScopeFactory(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public IMessageProducerScope Open(MessageScopeSettings messageScopeSettings)
        {
            return new MessageProducerScope(_connectionFactory, messageScopeSettings);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using RabbitMQ.Client;
using TaskManager.Common.QueueServiceBuilder.Interfaces;
using TaskManager.Common.QueueServiceBuilder.Models;

namespace TaskManager.Common.QueueServiceBuilder
{
    public class MessageConsumerScope : IMessageConsumerScope, IDisposable
    {
        private readonly MessageScopeSettings _messageScopeSettings;
        private readonly Lazy<IMessageQueueBuilder> _messageQueueLazy;
        private readonly Lazy<IMessageConsumer> _messageConsumerLazy;

        private readonly IConnectionFactory _connectionFactory;

        public MessageConsumerScope(IConnectionFactory connectionFactory, MessageScopeSettings messageScopeSettings)
        {
            _connectionFactory = connectionFactory;
            _messageScopeSettings = messageScopeSettings;

            _messageQueueLazy = new Lazy<IMessageQueueBuilder>(CreateMessageQueue);
            _messageConsumerLazy = new Lazy<IMessageConsumer>(CreateMessageConsumer);
        }

        public IMessageConsumer MessageConsumer => _messageConsumerLazy.Value;

        public IMessageQueueBuilder MessageQueueBuilder => _messageQueueLazy.Value;

        private IMessageConsumer CreateMessageConsumer()
        {
            return new MessageConsumer(new MessageConsumerSettings
            {
                Channel = MessageQueueBuilder.Channel,
                QueueName = _messageScopeSettings.QueueName
            });
        }

        private IMessageQueueBuilder CreateMessageQueue()
        {
            return new MessageQueueBuilder(_connectionFactory, _messageScopeSettings);
        }

        public void Dispose()
        {
            MessageQueueBuilder?.Dispose();
        }
    }
}

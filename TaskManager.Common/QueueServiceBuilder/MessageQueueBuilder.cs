﻿using System;
using System.Collections.Generic;
using System.Text;
using RabbitMQ.Client;
using TaskManager.Common.QueueServiceBuilder.Interfaces;
using TaskManager.Common.QueueServiceBuilder.Models;

namespace TaskManager.Common.QueueServiceBuilder
{
    public class MessageQueueBuilder: IMessageQueueBuilder, IDisposable
    {
        private readonly IConnection _connection;

        public IModel Channel { get; protected set; }

        public MessageQueueBuilder(IConnectionFactory connectionFactory)
        {
            _connection = connectionFactory.CreateConnection();
            Channel = _connection.CreateModel();
        }

        public MessageQueueBuilder(IConnectionFactory connectionFactory, MessageScopeSettings messageSettings)
        : this(connectionFactory)
        {
            DeclareExchange(messageSettings.ExchangeName, messageSettings.ExchangeType);

            if (messageSettings.QueueName != null)
            {
                BindQueue(messageSettings.QueueName, 
                    messageSettings.ExchangeName, 
                    messageSettings.RoutingKey);
            }
        }

        private void BindQueue(string queueName, string exchangeName, string routingKey)
        {
            Channel.QueueDeclare(queueName, true, false, false);
            Channel.QueueBind(queueName, exchangeName, routingKey);
        }

        private void DeclareExchange(string exchangeName, string exchangeType)
        {
            Channel.ExchangeDeclare(exchangeName, exchangeType);
        }

        public void Dispose()
        {
            Channel?.Dispose();
            _connection?.Dispose();
        }
    }
}

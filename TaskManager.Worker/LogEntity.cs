﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskManager.Worker
{
    public class LogEntity
    {
        public string Message { get; set; }
        public DateTime Date { get; set; }
    }
}

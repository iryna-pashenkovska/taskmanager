﻿using Microsoft.Extensions.Configuration;
using RabbitMQ.Client;
using System;
using System.IO;
using TaskManager.Common.QueueServiceBuilder;

namespace TaskManager.Worker
{
    class Program
    {
        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            IConfiguration configuration = builder.Build();

            IConnectionFactory factory = new Common.QueueServiceBuilder.ConnectionFactory(new Uri(configuration.GetSection("Rabbit").Value));

            MessageService messageService = new MessageService(new MessageConsumerScopeFactory(factory), new MessageProducerScopeFactory(factory));
            messageService.Run();
        }
    }
}

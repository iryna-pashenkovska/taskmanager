﻿using System.Collections.Generic;
using TaskManagerServer.Abstractions;
using TaskManagerServer.DTOs;

namespace TaskManagerServer.Queries
{
    public class GetTeamsQuery : IQuery<ICollection<Team>>
    {
    }
}

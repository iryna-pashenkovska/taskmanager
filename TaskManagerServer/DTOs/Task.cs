﻿using System;
using System.Reflection;
using System.Text;

namespace TaskManagerServer.DTOs
{
    public class Task
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
        public int State { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            var properties = typeof(Task).GetProperties();
            foreach (PropertyInfo property in properties)
            {
                sb.AppendLine(string.Format("{0}: {1}", property.Name, property.GetValue(this)));
            }

            sb.AppendLine();
            return sb.ToString();
        }
    }
}

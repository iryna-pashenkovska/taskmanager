﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TaskManager.Data.DataAccess;
using TaskManagerServer.Abstractions;
using TaskManagerServer.Queries;

namespace TaskManagerServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly IQueryProcessor _queryProcessor;
        private readonly ICommandProcessor _commandProcessor;
        private readonly IQueueService _queueService;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public TasksController(IQueryProcessor queryProcessor,
                               ICommandProcessor commandProcessor,
                               IQueueService queueService,
                               IUnitOfWorkFactory unitOfWorkFactory)
        {
            _queryProcessor = queryProcessor;
            _commandProcessor = commandProcessor;
            _queueService = queueService;
            _unitOfWorkFactory = unitOfWorkFactory;
        }
        // GET: api/TAsks
        [HttpGet]
        public async Task <ICollection<DTOs.Task>> Get()
        {
            if (_queueService.PostValue("Get all tasks called"))
            {
                return await _queryProcessor.ProcessAsync(new GetTasksQuery());
            }
            else
            {
                return null;
            }
        }

        //// GET: api/TAsks/5
        //[HttpGet("{id}", Name = "Get")]
        //public string Get(int id)
        //{
        //    return "value";
        //}

        // POST: api/TAsks
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/TAsks/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}

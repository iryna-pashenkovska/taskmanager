﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using TaskManager.Data.DataAccess;
using TaskManagerServer.Abstractions;
using TaskManagerServer.DTOs;
using TaskManagerServer.Queries;

namespace TaskManagerServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IQueryProcessor _queryProcessor;
        private readonly ICommandProcessor _commandProcessor;
        private readonly IQueueService _queueService;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public UsersController(IQueryProcessor queryProcessor,
            ICommandProcessor commandProcessor,
            IQueueService queueService,
            IUnitOfWorkFactory unitOfWorkFactory)
        {
            _queryProcessor = queryProcessor;
            _commandProcessor = commandProcessor;
            _queueService = queueService;
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        // GET: api/Users
        [HttpGet]
        public async Task<ICollection<User>> Get()
        {
            if (_queueService.PostValue("Get all users called"))
            {
                return await _queryProcessor.ProcessAsync(new GetUsersQuery());
            }
            else
            {
                return null;
            }
        }

        //// GET: api/Users/5
        //[HttpGet("{id}", Name = "Get")]
        //public string Get(int id)
        //{
        //    return "value";
        //}

        // POST: api/Users
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Users/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}

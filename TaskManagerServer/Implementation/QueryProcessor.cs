﻿using TaskManager.Data.DbContexts;
using TaskManagerServer.Abstractions;
using TaskManagerServer.Handlers;
using TaskManagerServer.Implementation;

namespace BasicApp.Services.Implementation
{
    public class QueryProcessor : Processor, IQueryProcessor
    {
        public QueryProcessor(IQueryHandlerFactory handlerFactory)
        {
            RegisterHandlers(handlerFactory);
        }
    }
}
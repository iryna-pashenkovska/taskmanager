﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TaskManagerServer.Abstractions;
using TaskManagerServer.Handlers;

namespace TaskManagerServer.Implementation
{
    public abstract class Processor
    {
        private Dictionary<Type, Func<object, Task<object>>> dictionary;

        public async Task<TResult> ProcessAsync<TResult>(ICommand<TResult> command) =>
            (TResult)await dictionary[command.GetType()](command);

        public async Task<TResult> ProcessAsync<TResult>(IQuery<TResult> command) =>
            (TResult)await dictionary[command.GetType()](command);

        protected void RegisterHandlers(IHandlerFactory commandHandlerFactory) =>
            dictionary = commandHandlerFactory.GetHandlers();
    }
}
﻿
using TaskManagerServer.Abstractions;
using TaskManagerServer.Handlers;

namespace TaskManagerServer.Implementation
{
    public class CommandProcessor : Processor, ICommandProcessor
    {
        public CommandProcessor(ICommandHandlerFactory handlerFactory)
        {
            RegisterHandlers(handlerFactory);
        }
    }
}
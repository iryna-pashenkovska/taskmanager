﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManager.Data.DataAccess;
using TaskManager.Data.DbContexts;
using TaskManagerServer.DTOs;
using TaskManagerServer.Queries;

namespace TaskManagerServer.Handlers
{
    public class QueryHandler : IQueryHandlerFactory
    {
        private readonly TaskManagerContext _dataContext;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IMapper _mapper;

        public QueryHandler(TaskManagerContext dataContext, 
                            IUnitOfWorkFactory unitOfWorkFactory, 
                            IMapper mapper)
        {
            _dataContext = dataContext;
            _unitOfWorkFactory = unitOfWorkFactory;
            _mapper = mapper;
        }

        public Dictionary<Type, Func<object, Task<object>>> GetHandlers()
        {
            return new Dictionary<Type, Func<object, Task<object>>>
            {
                { typeof(GetProjectQuery), async t => await HandleAsync(t as GetProjectQuery) },
                { typeof(GetTasksQuery), async t => await HandleAsync(t as GetTasksQuery) },
                { typeof(GetTeamsQuery), async t => await HandleAsync(t as GetTeamsQuery) },
                { typeof(GetUsersQuery), async t => await HandleAsync(t as GetUsersQuery) }
            };
        }

        private async Task<ICollection<Project>> HandleAsync(GetProjectQuery query)
        {
            using (var uow = _unitOfWorkFactory.Create(_dataContext))
            {
                var projects = await uow.Repository<TaskManager.Data.Entities.Project>().All();
                return projects.Select(_mapper.Map<TaskManager.Data.Entities.Project, Project>).ToList();
            }
        }

        private async Task<ICollection<DTOs.Task>> HandleAsync(GetTasksQuery query)
        {
            using (var uow = _unitOfWorkFactory.Create(_dataContext))
            {
                var tasks = await uow.Repository<TaskManager.Data.Entities.Task>().All();
                return tasks.Select(_mapper.Map<TaskManager.Data.Entities.Task, DTOs.Task>).ToList();
            }
        }

        private async Task<ICollection<Team>> HandleAsync(GetTeamsQuery query)
        {
            using (var uow = _unitOfWorkFactory.Create(_dataContext))
            {
                var teams = await uow.Repository<TaskManager.Data.Entities.Team>().All();
                return teams.Select(_mapper.Map<TaskManager.Data.Entities.Team, Team>).ToList();
            }
        }

        private async Task<ICollection<User>> HandleAsync(GetUsersQuery query)
        {
            using (var uow = _unitOfWorkFactory.Create(_dataContext))
            {
                var users = await uow.Repository<TaskManager.Data.Entities.User>().All();
                return users.Select(_mapper.Map<TaskManager.Data.Entities.User, User>).ToList();
            }
        }
    }
}
﻿using System.Threading.Tasks;
using System.Windows.Input;

namespace TaskManagerServer.Abstractions
{
    public interface ICommandHandler<in TCommand, TResult>
        where TCommand : ICommand<TResult>
        where TResult : new()
    {
        Task<TResult> HandleAsync(TCommand command);
    }
}
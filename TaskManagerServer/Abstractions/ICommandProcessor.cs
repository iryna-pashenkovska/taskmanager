﻿using System.Threading.Tasks;

namespace TaskManagerServer.Abstractions
{
    public interface ICommandProcessor
    {
        Task<TResult> ProcessAsync<TResult>(ICommand<TResult> command);
    }
}
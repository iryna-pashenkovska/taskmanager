﻿using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskManagerServer.Abstractions
{
    public interface IQueueService
    {
        bool PostValue(string message);
    }
}

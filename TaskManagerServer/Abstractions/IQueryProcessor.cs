﻿using System.Threading.Tasks;

namespace TaskManagerServer.Abstractions
{
    public interface IQueryProcessor
    {
        Task<TResult> ProcessAsync<TResult>(IQuery<TResult> query);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskManagerServer.Commands.Interfaces
{
    interface ICommandProjectService
    {
        Task SaveProject();
    }
}

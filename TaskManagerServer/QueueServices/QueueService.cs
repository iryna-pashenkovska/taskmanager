﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using TaskManager.Common.QueueServiceBuilder.Interfaces;
using TaskManager.Common.QueueServiceBuilder.Models;
using TaskManagerServer.Abstractions;
using TaskManagerServer.Hubs;

namespace TaskManagerServer.QueueServices
{
    public class QueueService: IQueueService
    {
        private readonly IMessageConsumerScope _messageConsumerScope;
        private readonly IMessageProducerScope _messageProducerScope;
        private readonly IHubContext<TaskManagerHub> _hubContext;

        public QueueService(IMessageConsumerScopeFactory messageConsumerScopeFactory,
            IMessageProducerScopeFactory messageProducerScopeFactory,
            IHubContext<TaskManagerHub> hubContext)
        {
            _hubContext = hubContext;

            _messageProducerScope = messageProducerScopeFactory.Open(new MessageScopeSettings
            {
                ExchangeName = "ServerExchange",
                ExchangeType = ExchangeType.Topic,
                QueueName = "SendValueQueue",
                RoutingKey = "topic.queue"
            });

            _messageConsumerScope = messageConsumerScopeFactory.Connect(new MessageScopeSettings
            {
                ExchangeName = "ClientExchange",
                ExchangeType = ExchangeType.Direct,
                QueueName = "SendResponceQueue",
                RoutingKey = "responce"
            });

            _messageConsumerScope.MessageConsumer.Received += GetValue;
        }

        public bool PostValue(string value)
        {
            try
            {
                _messageProducerScope.MessageProducer.Send(value);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        private void GetValue(object sender, BasicDeliverEventArgs args)
        {
            var value = Encoding.UTF8.GetString(args.Body);

            _hubContext.Clients.All.SendAsync("GetNotification", value);

            _messageConsumerScope.MessageConsumer.SetAcknowledge(args.DeliveryTag, true);
        }
    }
}

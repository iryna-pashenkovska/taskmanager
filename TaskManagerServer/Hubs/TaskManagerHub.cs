﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace TaskManagerServer.Hubs
{
    public class TaskManagerHub: Hub
    {
        public async Task GetNotification(string value)
        {
            await Clients.All.SendAsync("NewValue", value);
        }
    }
}

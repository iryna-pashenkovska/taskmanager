﻿using System;
using AutoMapper;
using BasicApp.Services.Implementation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RabbitMQ.Client;
using Swashbuckle.AspNetCore.Swagger;
using TaskManager.Common.QueueServiceBuilder;
using TaskManager.Common.QueueServiceBuilder.Interfaces;
using TaskManager.Data;
using TaskManager.Data.DataAccess;
using TaskManager.Data.DbContexts;
using TaskManager.Data.Entities;
using TaskManagerServer.Abstractions;
using TaskManagerServer.Handlers;
using TaskManagerServer.Hubs;
using TaskManagerServer.Implementation;
using TaskManagerServer.QueueServices;
using Task = TaskManager.Data.Entities.Task;

namespace TaskManagerServer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            services.AddScoped<IQueueService, QueueService>();
            //services.AddScoped<IUnitOfWorkFactory, UnitOfWorkFactory>();

            services.AddScoped<IMessageQueueBuilder, MessageQueueBuilder>();
            services.AddSingleton<IConnectionFactory>(x =>
                new TaskManager.Common.QueueServiceBuilder.ConnectionFactory(new Uri(Configuration.GetSection("Rabbit").Value)));

            services.AddScoped<IMessageProducer, MessageProducer>();
            services.AddScoped<IMessageProducerScope, MessageProducerScope>();
            services.AddScoped<IMessageProducerScopeFactory, MessageProducerScopeFactory>();

            services.AddScoped<IMessageConsumer, MessageConsumer>();
            services.AddScoped<IMessageConsumerScope, MessageConsumerScope>();
            services.AddScoped<IMessageConsumerScopeFactory, MessageConsumerScopeFactory>();

            services.AddCors();

            services.AddOptions();

            services.AddDbContext<TaskManagerContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("TaskManagerDatabase")));
            //.UseInMemoryDatabase(databaseName: "TaskManagerContext"));

            services.AddTransient<IUnitOfWorkFactory, UnitOfWorkFactory>();

            services.AddTransient<ICommandHandlerFactory, CommandHandlerFactory>();
            services.AddTransient<IQueryHandlerFactory, QueryHandler>();
            services.AddTransient<ICommandProcessor, CommandProcessor>();
            services.AddTransient<IQueryProcessor, QueryProcessor>();

            var mapper = MapperConfiguration().CreateMapper();
            services.AddScoped(_ => mapper);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "My API", Version = "v1" });
            });

            services.AddSignalR();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseSwagger();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1"); });

            app.UseSignalR(routes => { routes.MapHub<TaskManagerHub>("/taskmanagerhub"); });

            app.UseMvc();
        }

        public MapperConfiguration MapperConfiguration()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Project, DTOs.Project>();
                cfg.CreateMap<Task,DTOs.Task>();
                cfg.CreateMap<Team, DTOs.Team>();
                cfg.CreateMap<User, DTOs.User>();
            });

            return config;
        }
    }
}

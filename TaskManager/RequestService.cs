﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;
using TaskManager.Data.Entities;

namespace TaskManager
{
    public static class RequestService
    {
        public static async Task<List<Project>> GetProjects()
        {
            var projectsFromApi = await Program.client.GetStringAsync("api/projects");
            return JsonConvert.DeserializeObject<List<Project>>(projectsFromApi);
        }

        public static async Task<Project> GetProjectById(int projectId)
        {
            var projectFromApi = await Program.client.GetStringAsync(string.Format("api/projects/{0}", projectId));
            return JsonConvert.DeserializeObject<Project>(projectFromApi);
        }

        public static async Task<List<Data.Entities.Task>> GetTasks()
        {
            var tasksFromApi = await Program.client.GetStringAsync("api/tasks");
            return JsonConvert.DeserializeObject<List<Data.Entities.Task>>(tasksFromApi);
        }

        public static async Task<List<User>> GetUsers()
        {
            var usersFromApi = await Program.client.GetStringAsync("api/users");
            return JsonConvert.DeserializeObject<List<User>>(usersFromApi);
        }

        public static async Task<User> GetUserById(int id)
        {
            var userFromApi = await Program.client.GetStringAsync(string.Format("api/users/{0}", id));
            return JsonConvert.DeserializeObject<User>(userFromApi);
        }

        public static async Task<List<Team>> GetTeams()
        {
            var teamsFromApi = await Program.client.GetStringAsync("api/teams");
            return JsonConvert.DeserializeObject<List<Team>>(teamsFromApi);
        }

        //public static async Task<List<TaskState>> GetTaskStates()
        //{
        //    var taskStatesFromApi = await Program.client.GetStringAsync("api/taskstates");
        //    return JsonConvert.DeserializeObject<List<TaskState>>(taskStatesFromApi);
        //}
    }
}

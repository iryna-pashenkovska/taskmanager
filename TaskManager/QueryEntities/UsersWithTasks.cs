﻿using System.Collections.Generic;
using TaskManager.Data.Entities;

namespace TaskManager.QueryEntities
{
    public class UsersWithTasks
    {
        public User User { get; set; }
        public List<Task> Tasks { get; set; }
    }
}

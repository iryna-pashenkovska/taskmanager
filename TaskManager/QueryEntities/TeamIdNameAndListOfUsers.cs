﻿using System.Collections.Generic;
using TaskManager.Data.Entities;

namespace TaskManager.QueryEntities
{
    public class TeamIdNameAndListOfUsers
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public List<User> TeamMembers { get; set; }
    }
}

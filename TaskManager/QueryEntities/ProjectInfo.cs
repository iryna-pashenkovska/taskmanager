﻿using System.Reflection;
using System.Text;
using TaskManager.Data.Entities;

namespace TaskManager.QueryEntities
{
    public class ProjectInfo
    {
        public Project Project { get; set; }
        public Task LongestByDescriptionTask { get; set; }
        public Task ShortestByNameTask { get; set; }
        public int? OverallNumberOfUsersWithCondition { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            var properties = typeof(ProjectInfo).GetProperties();
            foreach (PropertyInfo property in properties)
            {
                sb.AppendLine(string.Format("{0}: {1}", property.Name, property.GetValue(this).ToString()));
            }

            sb.AppendLine();
            return sb.ToString();
        }
    }
}

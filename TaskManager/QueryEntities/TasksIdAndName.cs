﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskManager.QueryEntities
{
    public class TasksIdAndName
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}

﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;

namespace TaskManager
{
    class Program
    {
        public static HttpClient client = new HttpClient();

        static void Main(string[] args)
        {
            // Update port # in the following line.
            client.BaseAddress = new Uri("https://localhost:44371/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            var connection = new HubConnectionBuilder()
                .WithUrl("https://localhost:44371/TaskManagerHub")
                .Build();

            connection.Closed += async (error) =>
            {
                await Task.Delay(new Random().Next(0, 5) * 1000);
                await connection.StartAsync();
            };

            MenuHandler.DrawMenu();
            bool cont = true;
            do
            {
                try
                {
                    Console.WriteLine();
                    Console.Write("Your choise: ");
                    var action = Console.ReadLine();
                    Console.Clear();
                    MenuHandler.DrawMenu();
                    connection.On("GetNotification", () =>
                    {
                        Console.WriteLine("Check SignalR");
                    });
                    cont = MenuHandler.SelectMenuOption(action).Result;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            } while (cont);
        }
    }
}
